﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MDM.Core
{
	public class CameraLookTrigger : MonoBehaviour
	{
		public float triggerDistance = 100f;
		public bool debugRay = false;

		public LookAtTrigger activeHit; 
		// Update is called once per frame
		void FixedUpdate()
		{
			RaycastHit hitInfo;
			Ray ray = new Ray(transform.position, transform.forward);

			if (debugRay)
			{
				Debug.DrawRay(transform.position, transform.forward*triggerDistance, Color.yellow);
			}

			bool rayDidHit = Physics.Raycast(ray, out hitInfo, triggerDistance);

			if (rayDidHit)
			{
				activeHit = hitInfo.transform.gameObject.GetComponent<LookAtTrigger>();
				activeHit.OnLookAt(this);
			}
			else
			{
				activeHit = null;
			}
		}


	}

}