﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MDM.Core
{
    public class FastForward : MonoBehaviour
    {
        public KeyCode fastForward = KeyCode.Space;
        public float amount = 2f;

        // Update is called once per frame
        void LateUpdate()
        {
            if (Input.GetKey(fastForward))
            {
                Time.timeScale = amount;
            }
            else
            {
                Time.timeScale = 1f;
            }
        }
    }
}
