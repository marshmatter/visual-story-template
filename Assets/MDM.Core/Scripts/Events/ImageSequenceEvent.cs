﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace MDM.Core
{
    public class ImageSequenceEvent : BaseEvent
    {
        public SpriteRenderer[] imageSequence;
        public float startingOpacity = 0f;

		[Header("Fade In")]
        public Ease fadeInEase = Ease.Linear;
		public float defaultFadeInDuration = 1f;

        [Header("Duration")]
        public float durationAtFull = 1f;

        [Header("Fade Out")]
        public bool applyFadeOut = true;
		public Ease fadeOutEase = Ease.Linear;
		public float defaultFadeOutDuration = 1f;
        public float delayBeforeNextFade = 0f;

        [Header("All Images Completed")]
		public EventTrigger.EventTriggerData[] allCompletedEvents;

        int positionInSequence;
        Coroutine activeSequence;

        void Awake()
        {
            SetAllToAlpha(imageSequence, startingOpacity);
        }

        public void StartSequence(int _startingPosition = 0)
        {
            positionInSequence = _startingPosition;
            activeSequence = StartCoroutine(RunSequenceStep(_startingPosition));
        }

        public void StopSequence()
        {
            positionInSequence = 0;
            if (activeSequence != null)
            {
              StopCoroutine(activeSequence);  
            }
        }

        public void PauseSequence()
        {
			if (activeSequence != null)
			{
				StopCoroutine(activeSequence);
			}
        }

		IEnumerator RunSequenceStep(int _position)
		{
            if (defaultFadeInDuration > 0)
            {
				Tween fadeIn = imageSequence[_position].DOFade(1f, defaultFadeInDuration);
				// wait for fade in to complete
				yield return fadeIn.WaitForCompletion(); 
            }
            else
            {
                Color _c = imageSequence[_position].color;
                _c.a = 1f;
                imageSequence[_position].color = _c;
            }


            // wait for the duration at full to expire
            yield return new WaitForSeconds(durationAtFull);

            if (applyFadeOut)
            {
                if (defaultFadeOutDuration > 0)
                {
					Tween fadeOut = imageSequence[_position].DOFade(startingOpacity, defaultFadeOutDuration);

					yield return fadeOut.WaitForCompletion();
				}
                else
                {
					Color _c = imageSequence[_position].color;
					_c.a = 0f;
					imageSequence[_position].color = _c;
                }
				
            }
            if (delayBeforeNextFade > 0)
            {
                yield return new WaitForSeconds(delayBeforeNextFade);
            }


            if (imageSequence.Length > _position + 1)
            {
                StartSequence(_position + 1);
                // and it repeats
            }
            else
            {
                OnEventTriggered(allCompletedEvents);
                // sequence is over.
                activeSequence = null;
            }
		}

        void SetAllToAlpha(SpriteRenderer[] _imageList, float _alpha)
		{
            if (_imageList.Length == 0)
				return;

			foreach (SpriteRenderer sr in _imageList)
			{
				Color _c = sr.color;
                _c.a = _alpha;
				sr.color = _c;
			}
		}

    }
}
