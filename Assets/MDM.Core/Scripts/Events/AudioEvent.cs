﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MDM.Core
{
	[RequireComponent(typeof(AudioSource))]
	public class AudioEvent : BaseEvent
	{
		AudioSource source;

		[SerializeField]
		EventTrigger.EventTriggerData[] audioEndEvents;

		private void Awake()
		{
			source = GetComponent<AudioSource>();
		}

		public void PlayAudio()
		{
            OnEventTriggered();
			if (!source.isPlaying)
			{
				source.Play();
			}
		}

		public void StopAudio()
		{
			source.Stop();
		}

		public void MoveAudio(Transform _newPosition)
		{
			source.transform.SetPositionAndRotation(_newPosition.position, _newPosition.rotation);
		}

		IEnumerator CheckIfPlaying()
		{
			while(source.isPlaying)
			{
				yield return null;
			}
			OnEventTriggered(audioEndEvents);

		}

	}

}