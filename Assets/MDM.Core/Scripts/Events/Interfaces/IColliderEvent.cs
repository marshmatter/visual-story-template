﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MDM.Core
{
	public interface IColliderEvent
	{
		void OnLookAt(CameraLookTrigger _cameraTrigger);
	}

}