﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using DG.Tweening;
using UnityEngine.Playables;

namespace MDM.Core
{
	[RequireComponent(typeof(PlayableDirector))]
	public class TimelineEvent : BaseEvent
	{
		PlayableDirector director;

		[SerializeField]
		EventTrigger.EventTriggerData[] timelineEndEvents;

		private void Awake()
		{
			director = GetComponent<PlayableDirector>();
		}

		public void PlayTimeline()
		{
			OnEventTriggered();

			if (director == null)
			{
				Debug.Log("No Director found.");
				return;
			}

			director.Play();

			if (timelineEndEvents.Length > 0)
			{
				StartCoroutine(WaitForTimelineEnd());
			}
		}

		IEnumerator WaitForTimelineEnd()
		{
			while (director.state == PlayState.Playing)
			{
				yield return null;
			}

			OnEventTriggered(timelineEndEvents);
		}

		public void StopTimeline()
		{
			if (director == null)
			{
				return;
			}

			director.Stop();
		}
	}

}