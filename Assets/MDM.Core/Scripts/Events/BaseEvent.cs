﻿// full credit to Wayward and Kelly Wright for this Event-Trigger system

using UnityEngine;
using System.Collections;

namespace MDM.Core
{
	public class BaseEvent : MonoBehaviour
	{

		[SerializeField]
		public EventTrigger.EventTriggerData[] m_OnTriggeredEvents;
		[SerializeField]
		public bool BreakpointOnEvent = false;

		public void OnEventTriggered()
		{
			OnEventTriggered(m_OnTriggeredEvents);
		}

		protected void OnEventTriggered(EventTrigger.EventTriggerData[] data)
		{

			// if we're in the editor, allow the breakpoint to occur
#if UNITY_EDITOR
			if (BreakpointOnEvent)
			{
				Debug.Break();
				Debug.LogWarning("BREAKPOINT BY: " + gameObject.name);

				// focus on the breakpoint trigger in the scene view
				if (UnityEditor.SceneView.sceneViews.Count > 0)
				{
					UnityEditor.SceneView sceneView = (UnityEditor.SceneView)UnityEditor.SceneView.sceneViews[0];
					sceneView.LookAt(this.gameObject.transform.position);
				}
			}
#endif
			// nothing to trigger, exit method.. 
			if (data == null || data.Length == 0)
			{
				return;
			}

			// process the events. 
			for (int i = 0; i < data.Length; i++)
			{
				if (data[i].m_EventDelay > 0)
					StartCoroutine(DelayedEvent(data[i]));
				else
					data[i].m_Event.Invoke();
			}
		}

		public IEnumerator DelayedEvent(EventTrigger.EventTriggerData data)
		{
			yield return new WaitForSeconds(data.m_EventDelay);
			data.m_Event.Invoke();
		}
	} 
}