﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using DG.Tweening;

namespace MDM.Core
{
	[RequireComponent(typeof(VideoPlayer))]
	public class VideoEvent : BaseEvent
	{
		VideoPlayer video;

		[SerializeField]
		EventTrigger.EventTriggerData[] videoEndEvents;

		private void Awake()
		{
			video = GetComponent<VideoPlayer>();
		}

		public void PlayVideo()
		{
            OnEventTriggered();

			if (video == null)
			{
				Debug.Log("No VideoPlayer found.");
				return;
			}

			video.Play();

			if (videoEndEvents.Length > 0)
			{
				StartCoroutine(WaitForVideoEnd());
			}
		}

		IEnumerator WaitForVideoEnd()
		{
			while(video.isPlaying)
			{
				yield return null;
			}

			OnEventTriggered(videoEndEvents);
		}

		public void StopVideo()
		{
			if (video == null)
			{
				Debug.Log("No VideoPlayer found.");
				return;
			}

			video.Stop();
		}
	}

}