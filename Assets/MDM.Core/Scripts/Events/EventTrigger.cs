﻿// full credit to Wayward and Kelly Wright for this Event-Trigger system

using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System;

namespace MDM.Core
{
	public class EventTrigger : MonoBehaviour
	{
		[SerializeField, TextArea(3, 20)]
		string m_Notes;

		[SerializeField]
		private bool BreakpointOnEvent = false;
		private int eventsToTrigger;

		Action callback = null;

		public bool EventsDone
		{
			get
			{
				return eventsToTrigger <= 0;
			}
		}

		// Event class with optional delay
		[System.Serializable]
		public class EventTriggerData
		{
			[SerializeField]
			public float m_EventDelay;
			[SerializeField]
			public UnityEvent m_Event;
		}

		public void DoEvents(EventTriggerData[] events, Action cb)
		{
			DoEvents(events);
			callback = cb;
		}

		public void DoEvents(EventTriggerData[] events)
		{
			eventsToTrigger = 0;

#if UNITY_EDITOR
		if (BreakpointOnEvent)
		{
			Debug.Break();
			Debug.LogWarning("BREAKPOINT BY: " + gameObject.name);

			if (UnityEditor.SceneView.sceneViews.Count > 0)
			{
				UnityEditor.SceneView sceneView = (UnityEditor.SceneView)UnityEditor.SceneView.sceneViews[0];
				sceneView.LookAt(this.gameObject.transform.position);
			}
		}
#endif

			for (int i = 0; i < events.Length; i++)
			{
				eventsToTrigger++;
				if (events[i].m_EventDelay > 0f)
				{
					StartCoroutine(DelayedEvent(events[i]));
				}
				else
				{
					events[i].m_Event.Invoke();
					eventsToTrigger--;
				}
			}

			CheckDone();


		}

		IEnumerator DelayedEvent(EventTriggerData data)
		{
			yield return new WaitForSeconds(data.m_EventDelay);
			data.m_Event.Invoke();
			eventsToTrigger--;
			CheckDone();
		}

		void CheckDone()
		{
			if (EventsDone && callback != null)
			{
				callback();
			}
		}
	} 
}