﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MDM.Core
{
	[RequireComponent(typeof(Collider))]
	public class LookAtTrigger : BaseEvent, IColliderEvent
	{
		[SerializeField, TextArea(3, 20)]
		string m_Notes;

		Collider colliderLook;

		StateMonoBehaviour state;

		public float requiredLookTime;

		float lookTime = 0f;

		CameraLookTrigger cameraTrigger;

		public bool disableAfterTrigger;

		private void Awake()
		{
			state = gameObject.AddComponent<StateMonoBehaviour>();
			colliderLook = GetComponent<Collider>();
			lookTime = 0f;
		}

		private void Start()
		{
			state.SetState(Idle);
		}

		IEnumerator Idle()
		{
			while (true)
			{
				if (cameraTrigger == null)
				{

				}
				else
				{
					if (cameraTrigger.activeHit != this)
					{
						lookTime = 0f;
					}
				}

				yield return null;
			}
		}

		public void OnLookAt(CameraLookTrigger _cameraTrigger)
		{
			cameraTrigger = _cameraTrigger;

			if (disableAfterTrigger)
			{
				colliderLook.enabled = false;
			}

			if (requiredLookTime > 0 && lookTime < requiredLookTime)
			{
				lookTime += Time.deltaTime;
				return;
			}

			lookTime = 0f; 
			OnEventTriggered(m_OnTriggeredEvents);

		}

	}

}