﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MDM.Core
{
    public class StateChangeEvent : BaseEvent
    {
        public StateName defaultState;

        public enum StateName { ON, OFF};

		[Header("On")]
		public EventTrigger.EventTriggerData[] onEvents;

		[Header("Off")]
		public EventTrigger.EventTriggerData[] offEvents;

        bool currentState;

        private void Start()
        {
            SetState(defaultState == StateName.ON);
        }

        public void SetState(bool _state)
        {
            if (_state == true) // on
            {
                OnEventTriggered(onEvents);
            }
            else // off
            {
                OnEventTriggered(offEvents);
            }

            currentState = _state;
        }

        public void ToggleState()
        {
            SetState(!currentState);
        }
    }
}
