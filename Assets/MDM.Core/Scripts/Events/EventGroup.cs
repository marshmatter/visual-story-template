﻿// full credit to Wayward and Kelly Wright for this Event-Trigger system

using UnityEngine;
using System.Collections;

namespace MDM.Core
{
	public class EventGroup : EventTrigger
	{
		[SerializeField]
		EventTriggerData[] m_OnTriggerEvents;
		[SerializeField]
		bool m_TriggerOnStart;
        [SerializeField]
        bool m_TriggerOnEnable;

		void Start()
		{
			if (m_TriggerOnStart)
			{
				base.DoEvents(m_OnTriggerEvents);
			}
		}

        private void OnEnable()
        {
            if (m_TriggerOnEnable)
            {
                base.DoEvents(m_OnTriggerEvents);
            }
        }

        public void TriggerGroup()
		{
			base.DoEvents(m_OnTriggerEvents);
		}


	} 
}