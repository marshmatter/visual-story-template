﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace MDM.Core
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class SpriteFadeEvent : BaseEvent
    {
        [Header("Starting State")]
        public float startOpacity = 0.0f;

        [Header("Fade In")]
		public Ease fadeInEase = Ease.Linear;
		public float defaultFadeInDuration = 1f;
		public EventTrigger.EventTriggerData[] fadeInCompletedEvents;

        [Header("Fade Out")]
		public Ease fadeOutEase = Ease.Linear;
		public float defaultFadeOutDuration = 1f;
		public EventTrigger.EventTriggerData[] fadeOutCompletedEvents;


		SpriteRenderer source;

        private void Awake()
        {
            source = GetComponent<SpriteRenderer>();
			SetAlpha(startOpacity);
		}
		
		public void FadeIn()
		{
			FadeIn(defaultFadeInDuration);
		}

		public void FadeOut()
		{
			FadeOut(defaultFadeOutDuration);
		}

		public void FadeIn(float _duration)
        {
            Tween fade = source.DOFade(1f, _duration);
			StartCoroutine(
				WaitForFadeComplete(fade, fadeInCompletedEvents)
				);
        }

        public void FadeOut(float _duration)
        {
            Tween fade = source.DOFade(0f, _duration);
			StartCoroutine(
				WaitForFadeComplete(fade, fadeOutCompletedEvents)
				);
		}

		IEnumerator WaitForFadeComplete(Tween fade, EventTrigger.EventTriggerData[] events)
		{
			yield return fade.WaitForCompletion();

			if (events !=null || events.Length > 0)
			{
				OnEventTriggered(events);
			}

		}

        void SetAlpha(float _alpha)
        {
            if (source == null)
                return;

            Color _c = source.color;
            _c.a = _alpha;

            source.color = _c;
        }
    }
}
